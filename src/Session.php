<?php

namespace Kerekit\UnasApi;

class Session
{
    protected \DateTime $expire;

    /** @var string[] */
    protected array     $permissions;

    protected int       $shopId;
    protected string    $status;
    protected string    $token;

    public function __construct (array $data)
    {
        $this->expire      = $data ['expire'];
        $this->permissions = $data ['permissions'];
        $this->shopId      = $data ['shopId'];
        $this->status      = $data ['status'];
        $this->token       = $data ['token'];
    }

    public function __get (string $name)
    {
        $value = $this->{$name};
        return is_object ($value) ? (clone $value) : $value;
    }
}
