<?php

namespace Kerekit\UnasApi;

class Client
{
    public const URL_ROOT = 'https://api.unas.eu/shop/';

    protected string  $apiKey;
    protected Session $session;

    public function __construct (string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function __call (string $name, array $arguments)
    {
        return $this->_callAuthenticated ($name, $arguments [0]);
    }

    protected function _call (
        string $function,
        \SimpleXMLElement $input,
        $token = null
    ): \SimpleXMLElement
    {
        // Make request
        $headers = is_null ($token) ? [] : ["Authorization: Bearer $token"];
        $req = curl_init (self::URL_ROOT . $function);
        curl_setopt_array (
            $req,
            [
                CURLOPT_HEADER         => false,
                CURLOPT_HTTPHEADER     => $headers,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $input->asXML (),
                CURLOPT_RETURNTRANSFER => true,
            ]
        );
        $resStr = curl_exec ($req);
        if ($resStr === false) {
            throw new \Exception ("Failed to make login request.");
        }
        curl_close ($req);

        // Parse response
        $res = new \SimpleXMLElement ($resStr);
        if ($res === false) {
            throw new \Exception ("Failed to parse login response.");
        }

        return $res;
    }

    protected function _callAuthenticated (
        string $function,
        \SimpleXMLElement $input
    ): \SimpleXMLElement
    {
        // Login if needed
        $now = time ();
        if (
            !isset ($this->session) ||
            $this->session->expire->getTimestamp () <= $now
        ) {
            $this->_login ();
        }

        return $this->_call ($function, $input, $this->session->token);
    }

    /** @throws \Exception on failed login */
    protected function _login (): void
    {
        // Init input doc
        $params = new \SimpleXMLElement ('<?xml version="1.0" encoding="UTF-8"?><Params/>');
        $apiKey = $params->ApiKey = $this->apiKey;

        // Make login call
        $res = $this->_call ('login', $params);

        // Init session from data
        $expire = \DateTime::createFromFormat (
            'Y.m.d H:i:s',
            $res->Expire,
            new \DateTimeZone ('Europe/Budapest')
        );
        $permissions = [];
        foreach ($res->Permissions->Permission as $permission) {
            $permissions [] = "$permission";
        }
        $this->session = new Session ([
            'expire'      => $expire,
            'permissions' => $permissions,
            'shopId'      => (int) $res->ShopId,
            'status'      => "$res->Status",
            'token'       => "$res->Token",
        ]);
    }
}
