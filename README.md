# Unas API client

A basic, lightweight PHP client library for the API of Hungarian webshop rental system Unas.

Features (and lack of them):
- **Handles session automatically** (logs in on first request after init & after
  the session expires).
- Provides **all API functions & params by simple overloading** and requiring
  you to form the request body as `SimpleXMLElement` instances.
- **Parses response as `SimpleXMLElement` instance**, or throws a generic
  `Exception` if that fails for any reason.
- Note that the library does not check if a *valid* API response contains any
  errors. That's because while sending multiple items in a single request, the
  response might contain both successful and failed items. Throwing an error for
  such a mixed response would be misleading. Please take care to **check for
  success or error of your requests** in your code.
- The client does not respect **rate limiting**  automagically, you have to take
  care of it yourself in your own code as needed.

It seems reasonable to add enumerations and request/response type classes to the
library for convenience. Such features might roll out with a future version.

## Installation

Install the latest version with

```bash
$ composer require kerekit/unas-api
```

## Basic Usage

```php
<?php

// Init client by providing your API key
use Kerekit\UnasApi\Client as UnasApi;
$unasApi = new UnasApi ('your-api-key-comes-here');

// Get Unas blog posts (page contents with the type "blog")
$xml = new \SimpleXMLElement ('<?xml version="1.0" encoding="UTF-8"?><Params/>');
$xml->Type = 'blog';
$posts = $unasApi->getPageContent ($xml);

// Delete blog posts with title starting with "Obsolete post! "
$xml = new \SimpleXMLElement ('<?xml version="1.0" encoding="UTF-8"?><PageContents/>');
foreach ($posts->PageContent as $pageContent) {
    if (strpos ($pageContent->Title, 'Obsolete post! ') === 0) {
        $node         = $xml->addChild ('PageContent');
        $node->Action = 'delete';
        $node->Id     = $pageContent->Id;
    }
}
$delete = $unasApi->setPageContent ($xml);
$allPostsDeleted = true;
foreach ($res->PageContent as $pageContent) {
    if ('ok' !== "$pageContent->Status") {
        echo "ERROR! Failed to delete post #$pageContent->Id\n";
        $allPostsDeleted = false;
    }
}
if ($allPostsDeleted !== true) {
    exit (1);
}
echo "Successfully deleted all requested blog posts.\n";
```

See the official Unas API documentation for available functions and params:
https://unas.hu/tudastar/api

## Contribution
I'm all open for merging useful patches and improvements, but if you wanna roll
with me, you have to go old school. Please send your pull requests as classic
plain text emails to marton.tamas@webhelyesarcu.hu.
